package freemap.hikar.opencv

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.SurfaceView
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_main.*
import org.opencv.android.*

import org.opencv.core.Mat
import org.opencv.core.MatOfKeyPoint

import org.opencv.features2d.FastFeatureDetector

import java.util.*

class MainActivity : CameraActivity(), CameraBridgeViewBase.CvCameraViewListener2 {

    var fastFeatureDetector : FastFeatureDetector? = null

    var mLoaderCallback = object: BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS-> {
                    Log.i("hikar-opencv", "OpenCV loaded successfully");
                    fastFeatureDetector = FastFeatureDetector.create()
                    jcv1.enableView()
                }

                else -> {
                    super.onManagerConnected(status);
                }
            }
        }
    }

    companion object {
        init {
            System.loadLibrary("native_lib")
            System.loadLibrary("opencv_interface")
        }
    }


    external fun helloNative(): String
    external fun nativeAdd(arg1: Int, arg2: Int) : Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setContentView(R.layout.activity_main)
        jcv1.visibility = SurfaceView.VISIBLE
        jcv1.setCvCameraViewListener(this)
        val str = helloNative()
        AlertDialog.Builder(this).setPositiveButton("OK", null).setMessage("The native code says ${str} and adding 1 to 2 with native code gives: ${nativeAdd(1,2)}!").show()
        Log.d("hikar-opencv", str)
    }


    override fun onResume() {

        super.onResume()


        if (!OpenCVLoader.initDebug()) {
            Log.d("hikar-opencv", "Error initialising")
        } else {
            Log.d("hikar-opencv", "Initialised OK")
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }

    }

    override fun onPause() {
        super.onPause()
        jcv1?.disableView()
    }


    override fun onDestroy() {
        super.onDestroy()
        jcv1?.disableView()
    }

    override fun getCameraViewList(): MutableList<CameraBridgeViewBase> {
        return Collections.singletonList(jcv1)
    }

    override fun onCameraViewStarted(width: Int, height: Int) {

    }


    override fun onCameraViewStopped() {

    }


    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame): Mat {
        val img = inputFrame.gray()

        val addr = img.nativeObjAddr

        val keypoints = MatOfKeyPoint()
        fastFeatureDetector?.apply {
            detect(img, keypoints)
            Log.d("hikar-opencv", keypoints.toList().toString())
        }
        return img
    }
}
