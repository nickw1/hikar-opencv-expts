//
// Created by nick on 13/06/21.
//


#include <jni.h>
#include <string>
#include <sstream>

int native_add(int, int);

extern "C" JNIEXPORT jstring JNICALL
Java_freemap_hikar_opencv_MainActivity_helloNative(JNIEnv *env, jobject j_this) {
    std::string str = "Hello Native World!";
    return env->NewStringUTF(str.c_str());
}

extern "C" JNIEXPORT jint JNICALL Java_freemap_hikar_opencv_MainActivity_nativeAdd(JNIEnv *env, jobject j_this, int arg1, int arg2) {

//    std::stringstream sstream;
//    sstream << "The result is: " << native_add(arg1, arg2);
//    return env->NewStringUTF(sstream.str().c_str());
    return native_add(arg1, arg2);
}

int native_add(int a, int b) {
    return a + b;
}

